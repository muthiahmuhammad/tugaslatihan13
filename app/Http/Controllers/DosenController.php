<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DosenController extends Controller
{
    
    public function showForm()
    {
        return view('daftar');
    }

    
    public function welcome(Request $request)
    {
        $namaDepan = $request->input('nama_depan');
        $namaBelakang = $request->input('nama_belakang');
        $pesanSelamatDatang = "Selamat datang, $namaDepan $namaBelakang!";
        return view('welcome', ['pesan' => $pesanSelamatDatang]);
    }
       
        
    
    
}
