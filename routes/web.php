<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DosenController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home',[HomeController::class, 'utama']); 

Route::post('/regis',[AuthController::class,'regis']); 

Route::get('/register',[AuthController::class,'new']);


Route::get('/table',[HomeController::class, 'tab']); 




Route::get('/data-table', function () {
    return view('halaman.data-table');
});
